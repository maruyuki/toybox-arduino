// Configs
const int pinSpeaker = 12; // Pin number to connect piezo speaker
const int timer = 3; // Timer in minutes

// Tones
const int sol = 392;
const int la = 440;
const int si = 494;

void setup() {
  tone(pinSpeaker, si, 200);
  delay(200);
  tone(pinSpeaker, si, 200);
  delay(200);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000UL*60*timer);
  digitalWrite(LED_BUILTIN, HIGH);
  tone(pinSpeaker, sol, 300);
  delay(300);
  tone(pinSpeaker, la, 300);
  delay(300);
  tone(pinSpeaker, si, 1000);
  delay(1000);
  tone(pinSpeaker, la, 300);
  delay(300);
  tone(pinSpeaker, sol, 300);
  delay(600);
  tone(pinSpeaker, sol, 300);
  delay(300);
  tone(pinSpeaker, la, 300);
  delay(300);
  tone(pinSpeaker, si, 300);
  delay(300);
  tone(pinSpeaker, la, 300);
  delay(300);
  tone(pinSpeaker, sol, 300);
  delay(300);
  tone(pinSpeaker, la, 1200);
}

void loop() {
}
